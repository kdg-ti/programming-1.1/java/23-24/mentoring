package be.kdg.mentoring.week03;

public interface PersistenceManager {

    /**
     * This meethod should impelment the logic ncessary to store the provided <code>text</code>
     * into some persistence storage
     * @param text
     */
    void save(String text);

    /**
     * This method implements the logic to read text from the persitence storage
     *
     */
    String load();
}
