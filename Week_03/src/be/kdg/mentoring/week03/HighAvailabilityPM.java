package be.kdg.mentoring.week03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class HighAvailabilityPM extends AbstractPM {

    List<PersistenceManager> pms;

    @Override
    public void save(String text) {
        for (PersistenceManager pm: pms
             ) {
            pm.save(text);
        }
    }

    @Override
    public String load() {

        for (PersistenceManager pm: pms
        ) {
            String text = pm.load();
            if(text != null) {
                return text;
            }
        }
        return null;
    }
}
