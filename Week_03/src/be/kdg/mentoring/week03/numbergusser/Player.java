package be.kdg.mentoring.week03.numbergusser;

import java.util.Scanner;

public class Player {
    public Player(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public int guess() {
        return new Scanner(System.in).nextInt();
    }
}
