package be.kdg.mentoring.week03.numbergusser;

public class GameSession {
    private boolean playable;
    private int secretNumber;

    private final int maxAttempts;

    private final int numberOfPlayers;

    private int numberOfAttempts = 0;

    public GameSession(int secretNumber) {
        this(secretNumber, 5, 2);
    }

    public GameSession(int secretNumber, int maxAttempts, int numberOfPlayers) {
        this.secretNumber = secretNumber;
        this.maxAttempts = maxAttempts;
        this.numberOfPlayers = numberOfPlayers;
        this.playable = true;
    }

    public boolean isPlayable() {
        return playable && numberOfAttempts < maxAttempts*numberOfPlayers;
    }

    public void endGame() {
        this.playable = false;
    }

    public boolean isGuessCorrect(int guess) {
        numberOfAttempts++;
        return secretNumber == guess;

    }

    public int getSecretNumber() {
        return secretNumber;
    }


}
