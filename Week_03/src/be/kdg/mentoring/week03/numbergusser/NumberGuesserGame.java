package be.kdg.mentoring.week03.numbergusser;

import java.util.Random;

public class NumberGuesserGame {
    public static void main(String[] args) {


        GameSession session = new GameSession(new Random().nextInt(100));
        System.out.println(session.getSecretNumber());

        Player player1 = new Player("HumanPlayer");
        Player player2 = new AiPlayer("AiP Super Player");
        Player currentPlayer = player1;

        while(session.isPlayable()) {
            System.out.printf("%s please provide your guess: ", currentPlayer.getName());
            int guess = currentPlayer.guess();
            System.out.printf("%s guess is %d %n", currentPlayer.getName(),  guess);
            if(session.isGuessCorrect(guess)) {
                System.out.printf("%s has won", currentPlayer.getName());
                session.endGame();
            } else {
                // change turn
                if(currentPlayer == player1) {
                    currentPlayer = player2;
                } else {
                    currentPlayer = player1;
                }

//                currentPlayer = currentPlayer == player1 ? player2 : player1;
            }
        }
    }
}
