package be.kdg.mentoring.week03.numbergusser;

import java.util.Random;
import java.util.Scanner;

public class AiPlayer extends Player {

    public AiPlayer(String name) {
        super(name);
    }

    public int guess() {
        return new Random().nextInt(100);
    }
}
