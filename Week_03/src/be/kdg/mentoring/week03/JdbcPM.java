package be.kdg.mentoring.week03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcPM extends AbstractPM {

    private Connection connection;

    private String text;
    @Override
    public void save(String text) {

        try {
            PreparedStatement ps = connection.prepareStatement("Insert into text_table (text) values (?)");
            ps.setString(1, text);
            ps.execute();
            this.text = text;
            System.out.println("Text saved into the DB");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Problem while saving the text to the DB");
        }
    }

    @Override
    public String load() {
        try {
            PreparedStatement ps = connection.prepareStatement("Select from text_table");
            ResultSet rs = ps.executeQuery();
            System.out.println("Text loaded fromthe DB");
            if (rs.next())
                text = rs.getString(1);
            return text;


        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Problem while loading the text from the DB");
            return null;
        }
    }
}
