package be.kdg.mentoring.week03;

public class PersistenceDemo {
    public static void main(String[] args) {
        PersistenceManager persistenceManager = new JdbcPM();
        persistenceManager.save("Some secret");
        String text = persistenceManager.load();
        System.out.println(text);
    }
}
