package be.kdg.mentoring.week03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class FileSystemPM extends AbstractPM {

    private String text;
    @Override
    public void save(String text) {

        try {
            System.out.println("Storing the text into the file system");
            FileOutputStream out = new FileOutputStream("data.txt");
            out.write(text.getBytes());
            this.text = text;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String load() {
        try {
            log("Storing the text into the file system");
            Scanner scanner = new Scanner(new FileInputStream("data.txt"));
            text = scanner.nextLine();
            return text;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
