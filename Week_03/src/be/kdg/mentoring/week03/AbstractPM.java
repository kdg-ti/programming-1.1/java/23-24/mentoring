package be.kdg.mentoring.week03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;

public abstract class AbstractPM implements PersistenceManager {

    protected void log(String text) {
        System.out.println(text);
    }
}
