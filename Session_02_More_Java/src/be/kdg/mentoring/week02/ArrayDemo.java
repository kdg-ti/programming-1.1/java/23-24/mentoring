package be.kdg.mentoring.week02;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class ArrayDemo {
    public static void main(String[] args) {
        Number [] numbers = new Number[10];
        numbers[0] = 1;
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Math.random();
        }

        for (Number number: numbers) {
            System.out.println(number);
        }

        System.out.println(numbers[10]);

        ArrayList<Number> numbersAsArrayList = new ArrayList<>();


        for (Number number: numbers) {
            numbersAsArrayList.add(number);
        }
    }
}
