package be.kdg.mentoring.week02;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

public class Session {

    private final String sessionId;

    private LocalDateTime timestamp;

    public Session() {
        this(generateUUI());
    }

    public Session(String sessionId) {
        this.sessionId = setSessionId(sessionId);
        this.timestamp = LocalDateTime.now();
    }

    public String getSessionId() {
        return sessionId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public  String setSessionId(String sessionId) {
        if(sessionId != null && sessionId.length() >= 24) {
            return sessionId;
        }
        throw new RuntimeException("Invalid session id");
    }

    private static String generateSessionId() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder("S-");
        for (int i = 0; i < 24; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    public static String generateUUI() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String toString() {
        return "Session{" +
                "sessionId='" + sessionId + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
