package be.kdg.mentoring.week02;

public class SMSNotifier extends Notifier{
    public void notify(String text) {
        // Create an SMS merssage
        // use some SMS API to send a sms message.
        System.out.printf("Sending an sms to some one!%n", text);
    }
}
