package be.kdg.mentoring.week02;

public class AllNotifier extends Notifier{
    Notifier[] notifiers;

    public AllNotifier(Notifier[] notifiers) {
        this.notifiers = notifiers;
    }

    public void notify(String text) {

        System.out.printf("All Notifier sending message [%s]%n", text);
        for (Notifier notifier : notifiers) {
            notifier.notify(text);
        }
    }
}
