package be.kdg.mentoring.week02;

public class NoitifierDemo {
    public static void main(String[] args) {


        Notifier[] notifiers = new Notifier[2];

        notifiers[0] = new Notifier();
        notifiers[1] = new SMSNotifier();

//        Object[] objects = new Object[2];
//        objects[0] = "";
//        objects[1] = new Notifier();


        Notifier notifier = new AllNotifier(notifiers);

        notifier.notify("Cool message!");
    }
}
