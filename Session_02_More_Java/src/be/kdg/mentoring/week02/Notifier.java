package be.kdg.mentoring.week02;

public class Notifier {
    public void notify(String text) {
        System.out.printf("Sending a simple notification message [%s]%n", text);
    }
}
