package numberguesser;

import java.util.Scanner;

public class GuessingGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your name: ");
        String playerName = scanner.nextLine();
        Player player = new Player(playerName);

        GameSession session = new GameSession(5); // Max 5 attempts
        GameScore score = new GameScore();

        System.out.println(player.getName() + ", guess a number between 1 and 100. You have " + session.getRemainingAttempts() + " attempts.");

        while (session.getRemainingAttempts() > 0) {
            System.out.print("Enter your guess: ");
            int guess = scanner.nextInt();

            if (session.makeGuess(guess)) {
                System.out.println("Congratulations, " + player.getName() + "! You guessed the number!");
                score.updateScore(session.getRemainingAttempts());
                break;
            }

            if (session.getRemainingAttempts() == 0) {
                System.out.println("Game Over! The number was " + session.getSecretNumber());
            }
        }

        System.out.println("Your score: " + score.getScore());
    }
}