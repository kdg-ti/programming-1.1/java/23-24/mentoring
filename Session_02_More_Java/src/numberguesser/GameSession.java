package numberguesser;

import java.util.Random;

class GameSession {
    private final int secretNumber;
    private int remainingAttempts;

    public GameSession(int maxAttempts) {
        this(new Random().nextInt(100), maxAttempts);
    }

    public GameSession(int secretNumber, int remainingAttempts) {
        this.secretNumber = secretNumber;
        this.remainingAttempts = remainingAttempts;
    }

    // Method to make a guess and return if it was correct
    public boolean makeGuess(int guess) {
        remainingAttempts--;
        if (guess == secretNumber) {
            return true;
        } else {
            if (guess < secretNumber) {
                System.out.println("Too low! Try a higher number.");
            } else {
                System.out.println("Too high! Try a lower number.");
            }
            return false;
        }
    }

    // Method to get the number of remaining attempts
    public int getRemainingAttempts() {
        return remainingAttempts;
    }

    public int getSecretNumber() {
        return secretNumber;
    }
}