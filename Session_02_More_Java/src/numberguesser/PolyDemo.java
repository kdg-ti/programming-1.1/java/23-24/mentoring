package numberguesser;

public class PolyDemo {

    public static void main(String[] args) {


        Player player1 = new Player("Player1");

        Player player2 = new AiPlayer("Player2");

        AiPlayer aiPlayer = new AiPlayer("AiPlayer");

        player1.play();
        player2.play();
        aiPlayer.play();
    }
}
