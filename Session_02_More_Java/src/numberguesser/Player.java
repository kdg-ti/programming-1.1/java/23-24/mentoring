package numberguesser;

class Player {
    private String name;

    public Player(String name) {
        this.name = name;
    }

    // Method to get the player's name
    public String getName() {
        return name;
    }

    public void play() {
        System.out.println("Playing");
    }

    public void play(String text) {
        System.out.println("Playing " + text);
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                '}';
    }

//    @Override
//    public void nonExistingMethod() {
//
//    }
}
