package numberguesser;

public class AiPlayer extends Player{
    private int level;

    public AiPlayer(String name) {
        super(name);
        this.level = 1;
    }

    @Override
    public void play() {
        System.out.println("Very advanced play alogirithm");
    }

    @Override
    public String toString() {
        return "AiPlayer{" +
                "level=" + level +
                '}';
    }
}
