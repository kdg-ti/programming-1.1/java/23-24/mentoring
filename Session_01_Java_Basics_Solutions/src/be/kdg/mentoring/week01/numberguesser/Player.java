package be.kdg.mentoring.week01.numberguesser;

class Player {
    private String name;

    public Player(String name) {
        this.name = name;
    }

    // Method to get the player's name
    public String getName() {
        return name;
    }
}
