package be.kdg.mentoring.week01.numberguesser;

class GameScore {
    private int score;

    // Method to update the score based on remaining attempts
    public void updateScore(int remainingAttempts) {
        this.score += remainingAttempts * 10; // Example scoring logic
    }

    // Method to get the current score
    public int getScore() {
        return score;
    }
}

