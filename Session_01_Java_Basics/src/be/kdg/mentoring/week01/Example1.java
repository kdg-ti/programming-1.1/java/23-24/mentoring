package be.kdg.mentoring.week01;

public class Example1 {
    public static void main(String[] args) {
        int number1 = 100;
        double double2 = 3;

        int result = (int) (number1 + double2);

        System.out.println("100 + 3 = 103");

        System.out.println(number1 + " + " + double2 + " = " + result);
        System.out.println(number1 + " + " + double2 + " = " + result);

        System.out.printf("%d+%.1f=%d", number1, double2, result);
        System.out.printf("%d+%.1f=%d%n", number1, double2, result);

        String formattedString = """
                Dag iedereen    Hoe is het
                Hi there.. I am doing great!
                """;

        System.out.println(formattedString);


        System.out.println(100%25);

        System.out.println(100%51);

        // calcuate the total price of this productr
        double price = 23;
        double vat = .21;

        System.out.println(price * 0.21);
        System.out.println(price * 1- 0.21);
        System.out.println(price * (1- 0.21));
    }

}
