package be.kdg.mentoring.week01.numberguesser;

import java.util.Scanner;

public class GuessingGame {
    public static void main(String[] args) {
        // Game setup and logic

        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to our wonderful Guessing Game");


        String playerName = null;

        while(playerName == null || playerName.length() < 4) {
            System.out.println("Please enter your name: ");
            playerName = scanner.nextLine();
        }



        System.out.println("Thank you for providing a valid name, " + playerName);

        Player player = new Player();

        player.setPlayerName(playerName);


        System.out.println("Please enter your age: ");

        System.out.println(player);
    }
}