package be.kdg.mentoring.week01.numberguesser;

public class PlayerDemo {
    public static void main(String[] args) {
//        Player p = new Player(1, "333");
//        System.out.println(p.getPlayerName());
//        p.setPlayerName(null);
//        System.out.println(p.getPlayerName());

        Player player1 = new Player();
        System.out.println(Player.playerIdCounter + ", " +  player1.getPlayerId() + ", " + player1.getPlayerName());

        Player player2 = new Player();
        System.out.println(Player.playerIdCounter + ", " +  player2.getPlayerId() + ", " + player2.getPlayerName());

        System.out.println(Player.playerIdCounter);

        System.out.println(player1);

//        for (int i = 0; i < 10; i++) {
//            Player autoPlayer = new Player();
//            System.out.println(autoPlayer.playerIdCounter + ", " +  autoPlayer.getPlayerId() + ", " + autoPlayer.getPlayerName());
//        }
    }
}
