package be.kdg.mentoring.week01.numberguesser;

import java.util.Random;

class GameSession {
    private final int secretNumber;
    private int remainingAttempts;

    public GameSession(int secretNumber, int remainingAttempts) {
        this.secretNumber = secretNumber;
        this.remainingAttempts = remainingAttempts;
    }

    public boolean guess(int guess) {
        decrementRemainingGuesses();
        return guess == secretNumber;
    }

    private void decrementRemainingGuesses() {
        remainingAttempts--;
    }
}