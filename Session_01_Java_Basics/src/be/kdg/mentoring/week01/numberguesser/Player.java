package be.kdg.mentoring.week01.numberguesser;

class Player {
    private String playerName; // think of the naming convension. camelCase

    public static int playerIdCounter = 1;

    private final int playerId;

    public Player() {
        this(playerIdCounter++);
    }

    public Player(int playerId) {
        this(playerId, "DefaultPlayerName"); // calling the constructor with
    }

    public Player(int playerId, String playerName) {
        this.playerId = playerId;
        setPlayerName(playerName);
    }


    public int getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        if(playerName == null || playerName.length() < 4) {
            System.err.println("Invalid player name");
            System.out.println("Invalid player name");
            return;
        }
        // make sure that the player name has at least 4 character
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerName='" + playerName + '\'' +
                ", playerId=" + playerId +
                '}';
    }
}
